<?php

/**

 * Implements hook_views_data().
 */

function meeting_room_views_data() {
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['meeting_room']['table']['group'] = t('Meeting Room');
  
  // Define this as a base table – a table that can be described in itself by
  // views (and not just being brought in as a relationship). In reality this
  // is not very useful for this table, as it isn't really a distinct object of
  // its own, but it makes a good example.
  
  $data['meeting_room']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Meeting Room'),
    'help' => t('Meeting Room table contains example content and can be related to nodes.'),
    'weight' => -10,
  );
  $data['meeting_room']['table']['entity type'] = 'node';
  
  $data['meeting_room']['table']['default_relationship'] = array(
    'node' => array(
      'table' => 'node',
      'field' => 'nid',
     ),
  );  
  
  

  
  // ----------------------------------------------------------------
  // meeting_room table -- fields

  // nid
  $data['meeting_room']['nid'] = array(
    'title' => t('Nid'),
    'help' => t('The node ID.'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
    'relationship' => array(
        'title' => t('title'),
        'help' => t('Meeting Room Name'),
        'handler' => 'views_handler_relationship',
        'base' => 'node',
        'base field' => 'nid',
        'field' => 'nid',
        'label' => t('nodes'),    
    ),
  );
  
  
  // node - title
  $data['meeting_room']['node']['title'] = array(
    'title' => t('Meeting Room Name'),
    'help' => t('Meeting Room Name.'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),    
    'sort' => array(
    'handler' => 'views_handler_sort',
    ),
  );
  
  // description
  $data['meeting_room']['description'] = array(
    'title' => t('Description'),
    'help' => t('Meeting Room Description.'), // The help that appears on the UI,
    // Information for displaying the description
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),    
  );
  
  // location
  $data['meeting_room']['location'] = array(
    'title' => t('Location'),
    'help' => t('Meeting Room Location.'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),    
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
    'handler' => 'views_handler_sort',
    ),
  );
  
  // bgcolor
  $data['meeting_room']['bgcolor'] = array(
    'title' => t('BgColor'),
    'help' => t('Meeting Room BgColor.'), // The help that appears on the UI,
    // Information for displaying the bgcolor
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),    
  );
  
  // fgcolor
  $data['meeting_room']['fgcolor'] = array(
    'title' => t('FgColor'),
    'help' => t('Meeting Room FgColor.'), // The help that appears on the UI,
    // Information for displaying the fgcolor
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),    
  );
  
  // capacity
  $data['meeting_room']['capacity'] = array(
    'title' => t('Capacity'),
    'help' => t('Meeting Room Capacity.'), // The help that appears on the UI,
    // Information for displaying the capacity
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),    
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    )
  );
  // has_dvd
  $data['meeting_room']['has_dvd'] = array(
    'title' => t('DVD'),
    'help' => t('Meeting Room with DVD.'), // The help that appears on the UI,
    // Information for displaying if Meeting Room has DVD
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => FALSE, 
    ),   
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    )
  );
  
  // has_network
  $data['meeting_room']['has_network'] = array(
    'title' => t('Network'),
    'help' => t('Meeting Room with Network.'), // The help that appears on the UI,
    // Information for displaying if Meeting Room has Network
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => FALSE, 
    ),   
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    )
  );
  
  // has_pc
  $data['meeting_room']['has_pc'] = array(
    'title' => t('PC'),
    'help' => t('Meeting Room with PC.'), // The help that appears on the UI,
    // Information for displaying if Meeting Room has PC
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => FALSE, 
    ),   
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    )
  );
  
  // has_projector
  $data['meeting_room']['has_projector'] = array(
    'title' => t('Projector'),
    'help' => t('Meeting Room with Projector.'), // The help that appears on the UI,
    // Information for displaying if Meeting Room has Projector
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => FALSE, 
    ),   
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    )
  );
  
  // has_TV
  $data['meeting_room']['has_tv'] = array(
    'title' => t('TV'),
    'help' => t('Meeting Room with TV.'), // The help that appears on the UI,
    // Information for displaying if Meeting Room has TV
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => FALSE, 
    ),   
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    )
  );
  
  // has_WhiteBoard
  $data['meeting_room']['has_whiteboard'] = array(
    'title' => t('WhiteBoard'),
    'help' => t('Meeting Room with WhiteBoard.'), // The help that appears on the UI,
    // Information for displaying if Meeting Room has WhiteBoard
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => FALSE, 
    ),    
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    )
  );  
  
  
  return $data;  
  
}