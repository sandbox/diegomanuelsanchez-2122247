<?php

/**
 * Implements hook_views_default_views().
 * @return type
 */
function meeting_room_views_default_views() {
  
  $files = file_scan_directory(drupal_get_path('module', 'meeting_room').'/views', '/\.view$/');
  
  foreach ($files as $absolute => $file) {    
    require $absolute;
    if (isset($view)) {
      $views[$view->name] = $view;      
    }   
  } 
 
  return $views;
}
