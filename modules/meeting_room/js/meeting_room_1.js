(function ($) {
  Drupal.behaviors.colour2 = {
    attach: function(context) {
                var farb2 = $.farbtastic("#color_picker2");   
                farb2.linkTo("#edit-fgcolor"); 
                var color = $("#edit-fgcolor").val();
                farb2.setColor(color);                                
          }
   }    
 
})(jQuery);