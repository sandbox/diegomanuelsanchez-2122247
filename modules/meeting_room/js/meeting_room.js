(function ($) {
  Drupal.behaviors.colour1 = {
    attach: function(context) {
                var farb = $.farbtastic("#color_picker");   
                farb.linkTo("#edit-bgcolor"); 
                var color = $("#edit-bgcolor").val();
                farb.setColor(color);                                
          }
   }    
 
})(jQuery);